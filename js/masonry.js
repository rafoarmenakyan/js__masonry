function Masonry (DOMclass, sizing) {
    this.DOMclass = DOMclass,
    this.columnWidth = sizing.columnWidth
    this.autoResize = sizing.autoResize
}

Masonry.prototype.render = function(){
    const elements = Array.from(document.querySelectorAll(`${this.DOMclass} li`))
    let columnWidth = this.columnWidth
    let [elementsHeight,elementsWidthSum,elementMinHeight,minHeightIndex,elementFromLeft] = [[],0,0,0,0]
    elements.forEach((element,index) => {
        element.style.cssText = `width:${columnWidth}px`
        elementsWidthSum += element.offsetWidth
        if(elementsWidthSum <= window.innerWidth){
            elementsHeight.push(element.offsetHeight)
            element.style.cssText = `left:${elementsWidthSum-columnWidth}px; width:${columnWidth}px`
            return
        }
        elementMinHeight = Math.min(...elementsHeight)
        minHeightIndex = elementsHeight.indexOf(elementMinHeight)
        elementFromLeft = elements[minHeightIndex].style.left
        element.style.cssText = `top:${elementMinHeight}px; left:${elementFromLeft}; width:${columnWidth}px`
        elementsHeight[minHeightIndex] = elementsHeight[minHeightIndex] + element.offsetHeight
    })
}


window.addEventListener('resize', function(){
    let autoResize = masonry.autoResize;
    if(autoResize){
        masonry.render()
        return
    }
})